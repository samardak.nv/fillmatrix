package com.main

import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.assertThrows
import java.lang.IllegalArgumentException

class FillMatrixTest {

    @Test
    fun `size 8`() {
        val testedArray: Array<Array<Int>> = Array(8) { Array(8) { 0 } }
        val correctArray: Array<Array<Int>> = Array(8) { Array(8) { 0 } }
        correctArray[0] = arrayOf(21, 20, 10, 9, 3, 2, 0, 35)
        correctArray[1] = arrayOf(22, 19, 11, 8, 4, 1, 34, 36)
        correctArray[2] = arrayOf(23, 18, 12, 7, 5, 33, 48, 37)
        correctArray[3] = arrayOf(24, 17, 13, 6, 32, 49, 47, 38)
        correctArray[4] = arrayOf(25, 16, 14, 31, 57, 50, 46, 39)
        correctArray[5] = arrayOf(26, 15, 30, 58, 56, 51, 45, 40)
        correctArray[6] = arrayOf(27, 29, 62, 59, 55, 52, 44, 41)
        correctArray[7] = arrayOf(28, 63, 61, 60, 54, 53, 43, 42)
        assertArrayEquals(correctArray, fillMatrix(testedArray), "tested_array same as correct_array")
    }

    @Test
    fun `size 4`() {
        val testedArray: Array<Array<Int>> = Array(4) { Array(4) { 0 } }
        val correctArray: Array<Array<Int>> = Array(4) { Array(4) { 0 } }
        correctArray[0] = arrayOf(3, 2, 0, 9)
        correctArray[1] = arrayOf(4, 1, 8, 10)
        correctArray[2] = arrayOf(5, 7, 14, 11)
        correctArray[3] = arrayOf(6, 15, 13, 12)
        assertArrayEquals(correctArray, fillMatrix(testedArray), "tested_array same as correct_array")
    }

    @Test
    fun `size 2`() {
        val testedArray: Array<Array<Int>> = Array(2) { Array(2) { 0 } }
        val correctArray: Array<Array<Int>> = Array(2) { Array(2) { 0 } }
        correctArray[0] = arrayOf(0, 2)
        correctArray[1] = arrayOf(1, 3)
        assertArrayEquals(correctArray, fillMatrix(testedArray), "tested_array same as correct_array")
    }

    @Test
    fun `size 1`() {
        val testedArray: Array<Array<Int>> = Array(1) { Array(1) { 0 } }
        val correctArray: Array<Array<Int>> = Array(1) { Array(1) { 0 } }
        correctArray[0] = arrayOf(0)
        assertArrayEquals(correctArray, fillMatrix(testedArray), "tested_array same as correct_array")
    }


    @Test
    fun `size 0`() {
        val testedArray: Array<Array<Int>> = Array(0) { Array(0) { 0 } }
        assertThrows(NullPointerException::class.java) { fillMatrix(testedArray) }
    }

    @Test
    fun `not a square`() {
        val testedArray: Array<Array<Int>> = Array(4) { Array(2) { 0 } }
        assertThrows(IllegalArgumentException::class.java) { fillMatrix(testedArray) }
    }

}
