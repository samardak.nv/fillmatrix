package com.main

import java.lang.IllegalArgumentException

fun main() {
    var arr = Array(9) { Array(9) { 0 } }
    arr = fillMatrix(arr)
    for (i in 0 until arr.size) {
        for (j in 0 until arr.size) print(" ${arr[i][j]} ")
        print('\n')
    }
}

/**
 *  Вариант 12
 *  @param arr квадратный двумерный массив
 *  @return Array<Array<Int>> измененный по алгоритму квадратный массив
 */
fun fillMatrix(arr: Array<Array<Int>>): Array<Array<Int>> {

    if (arr.isEmpty()) throw NullPointerException("This array is empty")
    if (arr.size != arr[0].size) throw IllegalArgumentException("This array not a square")

    val n: Int = arr.size
    var x: Int
    var y: Int
    var i = 0
    var j = 0

    while (j < n - 1) {
        x = if (j % 2 == 1) {
            j
        } else {
            0
        }

        for (f in 0..j) {
            if (f != 0) {
                if (j % 2 == 1) {
                    x--
                } else {
                    x++
                }
            }
            y = n - j - 2
            arr[x][y] = i
            i++
        }
        j++
    }

    x = n - 1
    y = 0
    arr[x][y] = i
    i++

    for (f in 0 until n - 1) {
        x--
        y++
        arr[x][y] = i
        i++
    }

    j = 1

    while (j < n) {
        x = if (j % 2 == 1) {
            j
        } else {
            n - 1
        }
        for (f in 0 until n - j) {
            if (f != 0) {
                if (j % 2 == 1) {
                    x++
                } else {
                    x--
                }
            }
            y = n - j
            arr[x][y] = i
            i++
        }
        j++
    }

    return (arr)
}
